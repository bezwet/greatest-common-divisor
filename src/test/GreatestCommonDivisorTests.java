package test;

import main.GreatestCommonDivisor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GreatestCommonDivisorTests {

    @ParameterizedTest(name = "First number is {0}. Second number is {1}. Expected GCD is {2}")
    @CsvSource({"5,10, 5", "10,5, 5", "100,10,10", "25,25,25","0,10,10", "10,0,10"})
    public void getGCDUsingIteration_test(int numb1, int numb2, int expectedResult) {
        GreatestCommonDivisor greatestCommonDivisor = new GreatestCommonDivisor();
         int gcd = greatestCommonDivisor.getGCDUsingIteration(numb1, numb2);

        assertEquals(expectedResult,gcd);
    }

    @ParameterizedTest(name = "First number is {0}. Second number is {1}. Expected GCD is {2}")
    @CsvSource({"5,10, 5", "10,5, 5", "100,10,10", "25,25,25","0,10,10", "10,0,10"})
    public void getGCDUsingEuclidAlgorithmSubtraction_test(int numb1, int numb2, int expectedResult) {
        GreatestCommonDivisor greatestCommonDivisor = new GreatestCommonDivisor();
        int gcd = greatestCommonDivisor.getGCDUsingEuclidAlgorithmSubtraction(numb1, numb2);

        assertEquals(expectedResult,gcd);
    }

    @ParameterizedTest(name = "First number is {0}. Second number is {1}. Expected GCD is {2}")
    @CsvSource({"5,10, 5", "10,5, 5", "100,10,10", "25,25,25","0,10,10", "10,0,10"})
    public void getGCDUsingEuclidAlgorithmModulo_test(int numb1, int numb2, int expectedResult) {
        GreatestCommonDivisor greatestCommonDivisor = new GreatestCommonDivisor();
        int gcd = greatestCommonDivisor.getGCDUsingEuclidAlgorithmModulo(numb1, numb2);

        assertEquals(expectedResult,gcd);
    }
}
