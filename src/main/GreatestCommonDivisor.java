package main;

public class GreatestCommonDivisor {

    public int getGCDUsingIteration(int numb1, int numb2) {
        if (numb1 == 0) {
            return numb2;
        }
        if (numb2 == 0) {
            return numb1;
        }
        while (numb1 != numb2) {
            if (numb1 > numb2)
                numb1 -= numb2;
            else
                numb2 -= numb1;
        }
        return numb1;
    }

    public int getGCDUsingEuclidAlgorithmSubtraction(int numb1, int numb2) {
        if(numb1 == 0) {
            return numb2;
        }
        if (numb2 == 0) {
            return numb1;
        }
        if (numb1 == numb2) {
            return numb1;
        }
        if (numb1 > numb2) {
            return getGCDUsingEuclidAlgorithmSubtraction(numb2, numb1 - numb2);
        }
        return getGCDUsingEuclidAlgorithmSubtraction(numb1, numb2 - numb1);
    }

    public int getGCDUsingEuclidAlgorithmModulo(int numb1, int numb2) {
        if (numb2 == 0) {
            return numb1;
        }
        return getGCDUsingEuclidAlgorithmModulo(numb2, numb1 % numb2);
    }
}
