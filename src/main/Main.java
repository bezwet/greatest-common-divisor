package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Provide first number:");
        int numb1 = scanner.nextInt();
        System.out.println("Provide second number:");
        int numb2 = scanner.nextInt();


        GreatestCommonDivisor greatestCommonDivisor = new GreatestCommonDivisor();

        System.out.println("GCD using Iteration is: " + greatestCommonDivisor.getGCDUsingIteration(numb1, numb2));

        System.out.println("GCD using Euclid Algorithm with subtraction is: " + greatestCommonDivisor.getGCDUsingEuclidAlgorithmSubtraction(numb1, numb2));

        System.out.println("GCD using Euclid Algorithm with modulo is: " + greatestCommonDivisor.getGCDUsingEuclidAlgorithmModulo(numb1, numb2));

    }
}